const path = require('path');
// const ClosureCompiler = require('google-closure-compiler-js').webpack;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
	entry: [
		'./asset/scss/main.scss',
		'./asset/js/main.js'
	],
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'js/main.bundle.js'
	},
	target: 'web',
	devServer: {
		contentBase: path.resolve(__dirname, 'dist'),
		compress: true,
		port: 3000,
		stats: 'errors-only',
		hot: true,
		open: true,
		// noInfo: true,
	},
	module: {
		rules: [
			{
				test: /\.js$/, use: 'babel-loader', exclude: /node_modules/
			},
			{
				test: /\.(scss|css)$/,
				use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: { minimize: true }
						},
						'sass-loader'
					]
				}))
			},
			{
				test: /\.(png|jpg)$/,
				use: [
					'file-loader?name=[name].[ext]&outputPath=imgs/&publicPath=../',
					'image-webpack-loader'
				]
			},
			{
				test: /\.(svg|woff|woff2|eot|otf|TTF)$/,
				use: [
					'file-loader?name=[name].[ext]&outputPath=fonts/&publicPath=../'
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(['dist/*.js', 'dist/*.css']),
		// new ClosureCompiler({
		// 	options: {
		// 		languageIn: 'ECMASCRIPT6',
		// 		languageOut: 'ECMASCRIPT5',
		// 		compilationLevel: 'ADVANCED',
		// 		warningLevel: 'VERBOSE',
		// 	},
		// }),
		new ExtractTextPlugin('css/style.bundle.css'),
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin()
	],
	devtool: 'source-map'
};
