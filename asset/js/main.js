$(document).ready(function() {
    const sm = $(window).width();
    if (sm >= 991) {
        $('#fullpage').fullpage({
            'navigation': true,
            'navigationPosition': 'right',
            'navigationTooltips': ['Giới thiệu S-Group', 'Lộ trình đào tạo', 'Giới thiệu chuyên môn', 'Marketing Online', 'Thiết kế - Đa phương tiện', 'Lập trình', ' Truyền thông Marketing', 'Sale - Dự án', 'Nhân sự', 'Đăng ký tham gia'],
            'showActiveTooltip': true
        });
    }
});
